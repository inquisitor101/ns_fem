#!/bin/bash
# this is a CSS method script
cd ufl
ffc -O -l dolfin VelocityUpdate.ufl PressureUpdate.ufl PressureCorrection.ufl
cd ..
cmake .
make
./cavityFlow
rm -rf CMakeCache.txt cavityFlow Makefile cmake_install.cmake CMakeFiles/
exit 0
