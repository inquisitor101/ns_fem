#include <dolfin.h>

#define CSS  1 // NB: 0: CSS1, 1: CSS2

#include "ufl/VelocityUpdate.h"
#include "ufl/PressureCorrection.h"
#include "ufl/PressureUpdate.h"

using namespace dolfin;


// stationary walls
class NoSlipDomain : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
      return on_boundary &&
              (x[0] < DOLFIN_EPS || x[0] > 1.0 - DOLFIN_EPS || x[1] < DOLFIN_EPS);
  }
};

// cavity flow domain (top)
class InflowDomain : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
      return on_boundary &&
              (x[1] > 1.0 - DOLFIN_EPS);
  }
};

// inflow velocity profile
class InflowVelocity : public Expression
{
public:
  // constructor
  InflowVelocity() : Expression(2) {}
  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = sin(x[0]*DOLFIN_PI);
    values[1] = 0.0;
  }

  double t;
};

// corrected pressure boundary
class CorrectedPressureDomain : public SubDomain
{
  bool inside(const Array<double>&x, bool on_boundary) const
  {
    return on_boundary &&
            (x[0] < DOLFIN_EPS || x[0] > 1.0 - DOLFIN_EPS ||
             x[1] < DOLFIN_EPS || x[1] > 1.0 - DOLFIN_EPS );
  }
};


int main(int argc, char *argv[]){

  // set time related values
  double dt      = 0.01;
  double maxTime = 20.0;

  // create mesh
  auto mesh = std::make_shared<UnitSquareMesh>(30, 30);

  // set viscosity term (\nu)
  double viscosity = 0.001;

  // create function spaces
  auto V = std::make_shared<VelocityUpdate::FunctionSpace>(mesh);
  auto Q = std::make_shared<PressureUpdate::FunctionSpace>(mesh);

  // define values for the boundary conditions
  auto Vin = std::make_shared<InflowVelocity>();
  auto zero = std::make_shared<Constant>(0.0);
  auto zero_vector = std::make_shared<Constant>(0.0, 0.0);


  // define subdomains for boundary conditions
  auto noslip_domain = std::make_shared<NoSlipDomain>();
  auto inflow_domain = std::make_shared<InflowDomain>();
  auto corrpr_domain = std::make_shared<CorrectedPressureDomain>();

  // define boundary conditions
  DirichletBC noslip(V, zero_vector, noslip_domain);
  DirichletBC inflow(V, Vin, inflow_domain);

  // combine boundaries
  std::vector<DirichletBC*> bcu = { {&inflow, &noslip} };  // velocity BC
  std::vector<DirichletBC*> bcp = { };                     // pressure BC
  std::vector<DirichletBC*> bcc = { };

  // create functions
  auto u0     = std::make_shared<Function>(V); // u^{n-1}
  auto u1     = std::make_shared<Function>(V); // u^{ n }
  auto p0     = std::make_shared<Function>(Q); // p^{n-2}
  auto p1     = std::make_shared<Function>(Q); // p^{n-1}
  auto p_new  = std::make_shared<Function>(Q); // p_final
  auto psy    = std::make_shared<Function>(Q); // corrected pressure

  // create coefficients
  auto k    = std::make_shared<Constant>(dt);
  auto nu   = std::make_shared<Constant>(viscosity);
  auto f    = std::make_shared<Constant>(0.0, 0.0);

  // create forms
  VelocityUpdate::BilinearForm     a1(V, V);
  VelocityUpdate::LinearForm       L1(V);
  PressureCorrection::BilinearForm a2(Q, Q);
  PressureCorrection::LinearForm   L2(Q);
  PressureUpdate::BilinearForm     a3(Q, Q);
  PressureUpdate::LinearForm       L3(Q);

  // set coefficients (velocity)
  a1.k    = k;
  L1.k    = k;
  L1.u0   = u0;
  L1.p0   = p0;
  L1.p1   = p1;
  L1.f    = f;
  a1.nu   = nu;

  // set coefficients (corrected pressure)
  L2.k    = k;
  L2.u0   = u0;
  L2.u1   = u1;

  // set coefficients (pressure)
  L3.p0   = p0;
  L3.p1   = p1;
  L3.psy  = psy;
  L3.u1   = u1;
  L3.nu   = nu;

  // define matrices
  Matrix A1, A2, A3;

  // assemble matrices
  assemble(A1, a1);
  assemble(A2, a2);
  assemble(A3, a3);

  // create (rhs) vectors
  Vector b1, b2, b3;

  // create files for storing solution
  File ufile("results/velocity.pvd");
  File pfile("results/pressure.pvd");

  // assign starting time
  double t = dt;

  // define preconditioner (AMG)
  const std::string preconditioner(has_krylov_solver_preconditioner("amg") ? "amg" : "default");


  // iterate
  while (t < maxTime + DOLFIN_EPS){

    // update velocity boundary condition
    Vin->t = t;

    // step 1: compute velocity update
    begin("Computing velocity update");
    assemble(b1, L1);
    for (std::size_t i=0; i<bcu.size(); i++){
      bcu[i]->apply(A1, b1);
      bcu[i]->apply(*u1->vector());
    }
    solve(A1, *u1->vector(), b1, "gmres", "default");
    end();

    // step 2: compute pressure correction
    begin("Computing pressure correction");
    assemble(b2, L2);
    for (std::size_t i=0; i<bcc.size(); i++){
      bcc[i]->apply(A2, b2);
      bcc[i]->apply(*psy->vector());
    }
    solve(A2, *psy->vector(), b2, "bicgstab", preconditioner);
    end();

    // step 3: compute pressure update
    begin("Computing pressure update");
    assemble(b3, L3);
    for (std::size_t i=0; i<bcp.size(); i++){
      bcp[i]->apply(A3, b3);
      bcp[i]->apply(*p_new->vector());
    }
    solve(A3, *p_new->vector(), b3, "gmres", "default");
    end();

    // save to file
    ufile << *u1;
    pfile << *p_new;

    //  update values in time
    *u0 = *u1;
    *p0 = *p1;
    *p1 = *p_new;

    // update time step
    t  += dt;
    cout << "t= " << t << endl;

  }

  // plot solution
  plot(p1, "pressure");
  plot(u1, "velocity");
  interactive();

  // The End
  return 0;
}
