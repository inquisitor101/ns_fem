
#include <dolfin.h>

#include "ufl/TentativeVelocity.h"
#include "ufl/PressureUpdate.h"
#include "ufl/VelocityUpdate.h"


double Ymax = 0.41; // max height
double Xmax = 2.20; // max length
double Crad = 0.05; // cylinder radius
double cylX = 0.20; // cylinder's center horizontal position
double cylY = 0.20; // cylinder's center vertical   position
double Um   = 1.50; // uniform velocity (inflow parameter)


using namespace dolfin;

// define velocity no-slip boundary
class NoSlipDomain : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary &&
            (x[1] > Ymax - DOLFIN_EPS || x[1] < DOLFIN_EPS ||
              (x[1] < 0.3 && x[1] > 0.1 && x[0] < 0.3 && x[0] > 0.1) );
  }
};

// define inflow boundary
class InflowDomain : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary && x[0] < DOLFIN_EPS;
  }
};

// define outflow boundary
class OutflowDomain : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary && x[0] > Xmax - DOLFIN_EPS;
  }
};

// define pressure value at inflow boundary
class InflowVelocity : public Expression
{
public:

  // constructor
  InflowVelocity() : Expression(2) {}

  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = 4.0*(Um*(x[1]*(Ymax - x[1])))*sin(t*DOLFIN_PI/8.0)/(Ymax*Ymax);
    values[1] = 0.0;
  }

  // current time step
  double t;
};



int main(int argc, char *argv[]){

  // set viscosity term (\nu)
  double viscosity = 0.001;

  // create mesh
  auto mesh = std::make_shared<Mesh>("../cylinder.xml.gz");

  // set time related values
  double CFL     = 0.2;
  double h       = mesh->hmin();
  double Umax    = 3.5;
  double dt      = CFL*h/Umax;
  double maxTime = 8.0;

  //create function spaces
  auto V = std::make_shared<VelocityUpdate::FunctionSpace>(mesh);
  auto Q = std::make_shared<PressureUpdate::FunctionSpace>(mesh);

  // define values for the boundary conditions
  auto Vin         = std::make_shared<InflowVelocity>();
  auto zero        = std::make_shared<Constant>(0.0);
  auto zero_vector = std::make_shared<Constant>(0.0, 0.0);


  // define subdomains for boundary conditions
  auto noslip_domain  = std::make_shared<NoSlipDomain>();
  auto inflow_domain  = std::make_shared<InflowDomain>();
  auto outflow_domain = std::make_shared<OutflowDomain>();

  // define boundary conditions
  DirichletBC noslip(V, zero_vector, noslip_domain);
  DirichletBC inflow(V, Vin, inflow_domain);
  DirichletBC outflow(Q, zero, outflow_domain);

  // combine boundaries
  std::vector<DirichletBC*> bcu = { {&noslip, &inflow} };
  std::vector<DirichletBC*> bcp = { &outflow };

  // create functions
  auto u0 = std::make_shared<Function>(V);
  auto u1 = std::make_shared<Function>(V);
  auto p1 = std::make_shared<Function>(Q);

  // create coefficients
  auto k  = std::make_shared<Constant>(dt);
  auto nu = std::make_shared<Constant>(viscosity);
  auto f  = std::make_shared<Constant>(0.0, 0.0);

  // create forms
  TentativeVelocity::BilinearForm a1(V, V);
  TentativeVelocity::LinearForm   L1(V);
  PressureUpdate::BilinearForm    a2(Q, Q);
  PressureUpdate::LinearForm      L2(Q);
  VelocityUpdate::BilinearForm    a3(V, V);
  VelocityUpdate::LinearForm      L3(V);

  // set coefficients
  a1.k  = k;
  a1.nu = nu;

  L1.k  = k;
  L1.u0 = u0;
  L1.f  = f;

  L2.k  = k;
  L2.u1 = u1;

  L3.k  = k;
  L3.u1 = u1;
  L3.p1 = p1;

  // define matrices
  Matrix A1, A2, A3;

  // assemble matrices
  assemble(A1, a1);
  assemble(A2, a2);
  assemble(A3, a3);

  // create rhs vectors
  Vector b1, b2, b3;

  // create files for storing solution
  File ufile("results/velocity.pvd");
  File pfile("results/pressure.pvd");

  // assigne starting time
  double t = dt;

  // sample counter
  int iter = 0;

  // define preconditioner (AMG)
  const std::string preconditioner(has_krylov_solver_preconditioner("amg") ? "amg" : "default");

  // start timer
  tic();

  // iterate
  while (t < maxTime + DOLFIN_EPS){

    // update velocity boundary condition
    Vin->t = t;

    // step 1: compute tentative velocity
    begin("Computing tentative velocity");
      assemble(b1, L1);
      for (std::size_t i=0; i<bcu.size(); i++){
        bcu[i]->apply(A1, b1);
      }
      solve(A1, *u1->vector(), b1, "gmres", "default");
    end();

    // step 2: compute pressure correction
    begin("Computing pressure correction");
      assemble(b2, L2);
      for (std::size_t i=0; i<bcp.size(); i++){
        bcp[i]->apply(A2, b2);
        bcp[i]->apply(*p1->vector());
      }
      solve(A2, *p1->vector(), b2, "bicgstab", preconditioner);
    end();

    // step 3: compute velocity correction
    begin("Computing velocity correction");
      assemble(b3, L3);
      for (std::size_t i=0; i<bcu.size(); i++){
        bcu[i]->apply(A3, b3);
      }
      solve(A3, *u1->vector(), b3, "gmres", "default");
    end();

    // update time step
    *u0 = *u1;
    t  += dt;

    // save to file
    if (iter%10 == 0) {
      ufile << *u1;
      pfile << *p1;
    }

    // display time
    cout << "t= " << t << endl;

    // update iteration sample count
    iter++;
  }

  // return total execution time taken
  info("Code execution time: %e (seconds)", toc());

  // plot solution
  plot(p1, "pressure");
  plot(u1, "velocity");
  interactive();

  // The End
  return 0;
}
