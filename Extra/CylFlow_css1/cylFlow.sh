#!/bin/bash
# this script runs the IPCS method
cd ufl
ffc -O -l dolfin PressureCorrection.ufl PressureUpdate.ufl VelocityUpdate.ufl
cd ..
cmake .
make
./CylFlow
rm -rf CMakeCache.txt CylFlow Makefile cmake_install.cmake CMakeFiles/
exit 0
