#!/bin/bash
# this script runs the IPCS method
cd ufl
ffc -O -l dolfin TentativeVelocity.ufl PressureUpdate.ufl VelocityUpdate.ufl
cd ..
cmake .
make
./Lshape
rm -rf CMakeCache.txt Lshape Makefile cmake_install.cmake CMakeFiles/
exit 0
