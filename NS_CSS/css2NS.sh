#!/bin/bash
# this is a CSS method script
cd ufl/2d/CSS2
ffc -O -l dolfin VelocityUpdate.ufl PressureUpdate.ufl PressureCorrection.ufl StreamFunction.ufl
cd ../../3d/CSS2
ffc -O -l dolfin VelocityUpdate.ufl PressureUpdate.ufl PressureCorrection.ufl
cd ../../../CSS2
cmake .
make
./NS_css
rm -rf CMakeCache.txt NS_css Makefile cmake_install.cmake CMakeFiles/
exit 0
