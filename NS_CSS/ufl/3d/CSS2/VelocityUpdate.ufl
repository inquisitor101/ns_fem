# this is a ufl script for computing the tentative velocity

# define function spaces (P2-P1)
V = VectorElement("Lagrange", tetrahedron, 2)
Q = FiniteElement("Lagrange", tetrahedron, 1)

# define trial function
u = TrialFunction(V)

# define test function
v = TestFunction(V)

# define coefficients
k      = Constant(tetrahedron)
u0     = Coefficient(V)
p0     = Coefficient(Q)
p1     = Coefficient(Q)
f      = Coefficient(V)
nu     = Constant(tetrahedron)

# get geometric dimension
d = tetrahedron.geometric_dimension()

# define symmetric gradient
def epsilon(u):
    Du = grad(u)
    return 0.5*(Du + Du.T)

# define Cauchy stress tensor
def sigma(u, p):
    return 2*nu*epsilon(u) - p*Identity(d)

# select tentative pressure scheme
ps = 2*p1 - p0  # CSS 2

# define normal to the surface
n = FacetNormal(tetrahedron)

# equation
eq = (1/k)*inner(u - u0, v)*dx \
   + inner(grad(u0)*u0, v)*dx \
   + inner(sigma(u, ps), epsilon(v))*dx \
   - nu*inner(grad(u).T*n, v)*ds \
   - inner(f, v)*dx

# define bilinear form
a = lhs(eq)

# define linear form
L = rhs(eq)
