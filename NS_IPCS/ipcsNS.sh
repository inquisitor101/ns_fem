#!/bin/bash
# this script runs the IPCS method
cd ufl/2d
ffc -O -l dolfin TentativeVelocity.ufl PressureUpdate.ufl VelocityUpdate.ufl StreamFunction.ufl
cd ../3d
ffc -O -l dolfin TentativeVelocity.ufl PressureUpdate.ufl VelocityUpdate.ufl
cd ../..
cmake .
make
./NS_IPCS
rm -rf CMakeCache.txt NS_IPCS Makefile cmake_install.cmake CMakeFiles/
exit 0
