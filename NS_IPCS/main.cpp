
#define isSingleLidCavity 1
#define isTwoDimensional  1

#include <dolfin.h>

#if isTwoDimensional
  #include "ufl/2d/TentativeVelocity.h"
  #include "ufl/2d/PressureUpdate.h"
  #include "ufl/2d/VelocityUpdate.h"
#else
  #include "ufl/3d/TentativeVelocity.h"
  #include "ufl/3d/PressureUpdate.h"
  #include "ufl/3d/VelocityUpdate.h"
#endif

#include "ufl/2d/StreamFunction.h"

using namespace dolfin;

// -----------------------------------------------------------------------
#if isSingleLidCavity && isTwoDimensional

  class AllDomainBoundary : public SubDomain
  {
    bool inside(const Array<double>& x, bool on_boundary) const
    {
      return on_boundary;
    }
  };

#endif
// -----------------------------------------------------------------------



#if isTwoDimensional

  // stationary walls
  class NoSlipDomain : public SubDomain
  {
    bool inside(const Array<double>& x, bool on_boundary) const
    {
      #if isSingleLidCavity
        return on_boundary &&
                (x[0] < DOLFIN_EPS || x[0] > 1.0 - DOLFIN_EPS || x[1] < DOLFIN_EPS);
      #else // double lid-driven cavity flow
        return on_boundary &&
                (x[0] < DOLFIN_EPS || x[0] > 1.0 - DOLFIN_EPS);
      #endif
    }
  };

  // cavity flow domain (top)
  class InflowDomain : public SubDomain
  {
    bool inside(const Array<double>& x, bool on_boundary) const
    {
      #if isSingleLidCavity
        return on_boundary &&
                (x[1] > 1.0 - DOLFIN_EPS);
      #else
        return on_boundary &&
                (x[1] > 1.0 - DOLFIN_EPS || x[1] < DOLFIN_EPS);
      #endif
    }
  };

  // inflow velocity profile
  class InflowVelocity : public Expression
  {
  public:
    // constructor
    InflowVelocity() : Expression(2) {}

    void eval(Array<double>& values, const Array<double>& x) const
    {
      values[0] = 1.0;//-sin(x[1]*DOLFIN_PI);
      values[1] = 0.0;
    }

    // time (in case variable)
    double t;
  };

#else // three dimensions

  // stationary walls
  class NoSlipDomain : public SubDomain
  {
    bool inside(const Array<double>& x, bool on_boundary) const
    {
      #if isSingleLidCavity
        return on_boundary &&
                (x[0] < DOLFIN_EPS || x[0] > 1.0 - DOLFIN_EPS ||
                 x[1] < DOLFIN_EPS || x[1] > 1.0 - DOLFIN_EPS || x[2] < DOLFIN_EPS);
      #else // double lid-driven cavity flow
        return on_boundary &&
                (x[0] < DOLFIN_EPS || x[0] > 1.0 - DOLFIN_EPS ||
                 x[1] < DOLFIN_EPS || x[1] > 1.0 - DOLFIN_EPS);
      #endif
    }
  };

  // cavity flow domain (top)
  class InflowDomain : public SubDomain
  {
    bool inside(const Array<double>& x, bool on_boundary) const
    {
      #if isSingleLidCavity
        return on_boundary &&
                (x[2] > 1.0 - DOLFIN_EPS);
      #else
        return on_boundary &&
                (x[2] > 1.0 - DOLFIN_EPS || x[2] < DOLFIN_EPS);
      #endif
    }
  };

  // inflow velocity profile
  class InflowVelocity : public Expression
  {
  public:
    // constructor
    InflowVelocity() : Expression(3) {}

    void eval(Array<double>& values, const Array<double>& x) const
    {
      values[0] = 1.0;//-sin(x[1]*DOLFIN_PI);
      values[1] = 0.0;
      values[2] = 0.0;
    }

    // time (in case variable)
    double t;
  };

#endif

int main(int argc, char *argv[]){

  // set time related values
  double dt 	 = 0.01;
  double maxTime = 2.50;

  // set viscosity term (\nu)
  double viscosity = 0.001;

  // create mesh
#if isTwoDimensional
  auto mesh = std::make_shared<UnitSquareMesh>(24, 24);
#else
  auto mesh = std::make_shared<UnitCubeMesh>(6, 6, 6);
#endif

  //create function spaces
  auto V = std::make_shared<VelocityUpdate::FunctionSpace>(mesh);
  auto Q = std::make_shared<PressureUpdate::FunctionSpace>(mesh);

  // define values for the boundary conditions
  auto Vin = std::make_shared<InflowVelocity>();
  auto zero = std::make_shared<Constant>(0.0);
#if isTwoDimensional
  auto zero_vector = std::make_shared<Constant>(0.0, 0.0);
#else
  auto zero_vector = std::make_shared<Constant>(0.0, 0.0, 0.0);
#endif

  // define subdomains for boundary conditions
  auto noslip_domain = std::make_shared<NoSlipDomain>();
  auto inflow_domain = std::make_shared<InflowDomain>();

  // define boundary conditions
  DirichletBC noslip(V, zero_vector, noslip_domain);
  DirichletBC inflow(V, Vin, inflow_domain);

  // combine boundaries
  std::vector<DirichletBC*> bcu = { {&inflow, &noslip} };
  std::vector<DirichletBC*> bcp = { };

  // create functions
  auto u0 = std::make_shared<Function>(V);
  auto u1 = std::make_shared<Function>(V);
  auto p0 = std::make_shared<Function>(Q);
  auto p1 = std::make_shared<Function>(Q);

  // create coefficients
  auto k  = std::make_shared<Constant>(dt);
  auto nu = std::make_shared<Constant>(viscosity);
#if isTwoDimensional
  auto f = std::make_shared<Constant>(0.0, 0.0);
#else
  auto f = std::make_shared<Constant>(0.0, 0.0, 0.0);
#endif

  // create forms
  TentativeVelocity::BilinearForm a1(V, V);
  TentativeVelocity::LinearForm   L1(V);
  PressureUpdate::BilinearForm    a2(Q, Q);
  PressureUpdate::LinearForm      L2(Q);
  VelocityUpdate::BilinearForm    a3(V, V);
  VelocityUpdate::LinearForm      L3(V);

  // set coefficients
  a1.k  = k;
  a1.nu = nu;
  L1.k  = k;
  L1.u0 = u0;
  L1.f  = f;
  L1.p0 = p0;
  L1.nu = nu;

  L2.k  = k;
  L2.p0 = p0;
  L2.u1 = u1;

  L3.k  = k;
  L3.u1 = u1;
  L3.p0 = p0;
  L3.p1 = p1;

  // define matrices
  Matrix A1, A2, A3;

  // assemble matrices
  assemble(A1, a1);
  assemble(A2, a2);
  assemble(A3, a3);

  // create rhs vectors
  Vector b1, b2, b3;

  // create files for storing solution
  File ufile("results/velocity.pvd");
  File pfile("results/pressure.pvd");

  // assigne starting time
  double t = dt;

  // start timer
  tic();

  // iterate
  while (t < maxTime + DOLFIN_EPS){

    // update velocity boundary condition
    Vin->t = t;

    // step 1: compute tentative velocity
    begin("Computing tentative velocity");
    assemble(b1, L1);
    for (std::size_t i=0; i<bcu.size(); i++){
      bcu[i]->apply(A1, b1);
    }
    solve(A1, *u1->vector(), b1);
    end();

    // step 2: compute pressure correction
    begin("Computing pressure correction");
    assemble(b2, L2);
    for (std::size_t i=0; i<bcp.size(); i++){
      bcp[i]->apply(A2, b2);
      bcp[i]->apply(*p1->vector());
    }
    solve(A2, *p1->vector(), b2);
    end();

    // step 3: compute velocity correction
    begin("Computing velocity correction");
    assemble(b3, L3);
    for (std::size_t i=0; i<bcu.size(); i++){
      bcu[i]->apply(A3, b3);
    }
    solve(A3, *u1->vector(), b3);
    end();

    // save to file
    ufile << *u1;
    pfile << *p1;

    // update time step
    *u0 = *u1;
    *p0 = *p1;
    t  += dt;
    cout << "t= " << t << endl;

  }

  // return total execution time taken
  info("Code execution time: %e (seconds)", toc());

  // calculate stream function - error check !
  // -----------------------------------------------------
  #if isSingleLidCavity && isTwoDimensional

    auto VV = std::make_shared<StreamFunction::FunctionSpace>(mesh);

    // create forms
    StreamFunction::BilinearForm a_(VV, VV);
    StreamFunction::LinearForm   L_(VV);

    // specify solution vector
    L_.u = u1;

    // define boundary conditions
    auto boundary = std::make_shared<AllDomainBoundary>();
    DirichletBC bc_(VV, zero, boundary);

    // create solution file
    File sfile("results/streamFunction_Error.pvd");
    Function psi(VV);

    // solve and save
    solve( a_ == L_, psi, bc_);
    sfile << psi;
  #endif
  // -----------------------------------------------------

  // plot solution
  plot(p1, "pressure");
  plot(u1, "velocity");
  interactive();

  // The End
  return 0;
}
