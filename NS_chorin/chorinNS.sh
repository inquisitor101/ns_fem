#!/bin/bash
# this is a Chorin's projection method script
cd ufl/2d
ffc -O -l dolfin TentativeVelocity.ufl PressureUpdate.ufl VelocityUpdate.ufl StreamFunction.ufl
cd ../3d
ffc -O -l dolfin TentativeVelocity.ufl PressureUpdate.ufl VelocityUpdate.ufl
cd ../..
cmake .
make
./NS_chorin
rm -rf CMakeCache.txt NS_chorin Makefile cmake_install.cmake CMakeFiles/
exit 0
