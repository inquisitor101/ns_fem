#!/bin/bash
# this script runs the stokes system
cd ufl/3d
ffc -O -l dolfin Stokes.ufl
cd ../2d
ffc -O -l dolfin Stokes.ufl
cd ../..
cmake .
make
./stationaryStokes
rm -rf CMakeCache.txt stationaryStokes Makefile cmake_install.cmake CMakeFiles/
exit 0
