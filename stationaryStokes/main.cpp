#include <dolfin.h>

#define ThreeDimensional 1
#define SingleCavityFlow 1

#if ThreeDimensional
  #include "ufl/3d/Stokes.h"
#else
  #include "ufl/2d/Stokes.h"
#endif

using namespace dolfin;


#if SingleCavityFlow

  #if ThreeDimensional

    // stationary walls
    class NoSlipDomain : public SubDomain
    {
      bool inside(const Array<double>& x, bool on_boundary) const
      {
        return on_boundary &&
                (x[0] < DOLFIN_EPS || x[0] > 1.0 - DOLFIN_EPS ||
                 x[1] < DOLFIN_EPS || x[1] > 1.0 - DOLFIN_EPS || x[2] < DOLFIN_EPS);
      }
    };

    // top lid-driven walls
    class InflowDomain : public SubDomain
    {
      bool inside(const Array<double>& x, bool on_boundary) const
      {
        return on_boundary && \
                (x[2] > 1.0 - DOLFIN_EPS);
      }
    };

    // define inflow velocity profile
    class InflowVelocity : public Expression
    {
    public:
      // constructor
      InflowVelocity() : Expression(3) {}

      void eval(Array<double>& values, const Array<double>& x) const
      {
        values[0] = 1.0; //-sin(x[1]*DOLFIN_PI);
        values[1] = 0.0;
        values[2] = 0.0;
      }
    };

  #else // two dimensions

    // stationary walls
    class NoSlipDomain : public SubDomain
    {
      bool inside(const Array<double>& x, bool on_boundary) const
      {
        return on_boundary && \
                (x[0] < DOLFIN_EPS || x[0] > 1.0 - DOLFIN_EPS || x[1] < DOLFIN_EPS);
      }
    };

    // top lid-driven wall
    class InflowDomain : public SubDomain
    {
      bool inside(const Array<double>& x, bool on_boundary) const
      {
        return on_boundary && \
                (x[1] > 1.0 - DOLFIN_EPS);
      }
    };

    // define inflow velocity profile
    class InflowVelocity : public Expression
    {
    public:
      // constructor
      InflowVelocity() : Expression(2) {}

      void eval(Array<double>& values, const Array<double>& x) const
      {
        values[0] = 1.0; //-sin(x[1]*DOLFIN_PI);
        values[1] = 0.0;
      }
    };

  #endif // two dimensions

#else // double cavity flow

  #if ThreeDimensional

    // stationary walls
    class NoSlipDomain : public SubDomain
    {
      bool inside(const Array<double>& x, bool on_boundary) const
      {
        return on_boundary &&
                (x[0] < DOLFIN_EPS || x[0] > 1.0 - DOLFIN_EPS ||
                 x[1] < DOLFIN_EPS || x[1] > 1.0 - DOLFIN_EPS);
      }
    };

    // top lid-driven walls
    class InflowDomain : public SubDomain
    {
      bool inside(const Array<double>& x, bool on_boundary) const
      {
        return on_boundary && \
                (x[2] > 1.0 - DOLFIN_EPS || x[2] < DOLFIN_EPS);
      }
    };

    // define inflow velocity profile
    class InflowVelocity : public Expression
    {
    public:
      // constructor
      InflowVelocity() : Expression(3) {}

      void eval(Array<double>& values, const Array<double>& x) const
      {
        values[0] = x[2] < DOLFIN_EPS ? 1.0 : 1.0; //-sin(x[1]*DOLFIN_PI);
        values[1] = 0.0;
        values[2] = 0.0;
      }
    };

  #else // two dimensions

    class NoSlipDomain : public SubDomain
    {
      bool inside(const Array<double>& x, bool on_boundary) const
      {
        return on_boundary && \
                (x[0] < DOLFIN_EPS || x[0] > 1.0 - DOLFIN_EPS);
      }
    };

    // top-bottom lid-driven walls
    class InflowDomain : public SubDomain
    {
      bool inside(const Array<double>& x, bool on_boundary) const
      {
        return on_boundary && \
                (x[1] > 1.0 - DOLFIN_EPS || x[1] < DOLFIN_EPS);
      }
    };

    // define inflow velocity profile
    class InflowVelocity : public Expression
    {
    public:
      // constructor
      InflowVelocity() : Expression(2) {}

      void eval(Array<double>& values, const Array<double>& x) const
      {
        values[0] = x[1] < DOLFIN_EPS ? 1.0 : 1.0; //-sin(x[1]*DOLFIN_PI);
        values[1] = 0.0;
      }
    };

  #endif // two dimensions

#endif // single/double cavity flow



int main(int argc, char *argv[]){

  // create mesh
#if ThreeDimensional
  auto mesh = std::make_shared<UnitCubeMesh>(10, 10, 10);
#else
  auto mesh = std::make_shared<UnitSquareMesh>(24, 24);
#endif
  // create function space
  auto W = std::make_shared<Stokes::FunctionSpace>(mesh);


  // set-up velocity inflow BC
  auto inflow_profile = std::make_shared<InflowVelocity>();
  auto top = std::make_shared<InflowDomain>();
  // auto inflow = std:: make_shared<DirichletBC>(W->sub(0), inflow_profile, top);
  DirichletBC inflow(W->sub(0), inflow_profile, top);

  // set-up stationary no-slip BC
#if ThreeDimensional
  auto noslip_profile = std::make_shared<Constant>(0.0, 0.0, 0.0);
#else
  auto noslip_profile = std::make_shared<Constant>(0.0, 0.0);
#endif
  auto stationary = std::make_shared<NoSlipDomain>();
  // auto noslip = std::make_shared<DirichletBC>(W->sub(0), noslip_profile, stationary);
  DirichletBC noslip(W->sub(0), noslip_profile, stationary);

  // combine BC
  std::vector<const DirichletBC*> bcu = { {&inflow, &noslip} };

  // create source
#if ThreeDimensional
  auto f = std::make_shared<Constant>(0.0, 0.0, 0.0);
#else
  auto f = std::make_shared<Constant>(0.0, 0.0);
#endif

  // define bilinear and linear forms
  Stokes::BilinearForm a(W, W);
  Stokes::LinearForm   L(W);

  // assign source
  L.f = f;

  // create solution vector
  Function w(W);

  // compute solution
  solve( a == L, w, bcu);

  // create files
  File ufile("results/velocity.pvd");
  File pfile("results/pressure.pvd");

  // split solution
  Function u = w[0];
  Function p = w[1];

  // write solution to files
  ufile << u;
  pfile << p;

  // plot solution
  plot(u, "velocity");
  plot(p, "pressure");
  plot(mesh);
  interactive();


  // The End
  return 0;
}
